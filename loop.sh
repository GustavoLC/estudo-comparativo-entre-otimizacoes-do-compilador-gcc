#!/bin/bash

files=/Users/you/dev/*

# Especifica o formato desejado para a saida do comando time
TIMEFORMAT=%R

# Compila 30 vezes o programa enquanto imprime o seu tempo de compilação
echo Compilação:
for (( i=0; i<30; i++ ))
do
  time gcc main.c -lm 
done

# Executa 30 vezes o programa, jogando os resultados em um .txt
# enquanto imprime o seu tempo de execução
 echo Execução:
for (( i=0; i<30; i++ ))
do
  time ./a.out > saida.txt
done