#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

// Código para gerar um número aleatório entre um mínimo e um máximo
float sortearNumero(float min, float max)
{

    return min + fmod(rand(), (max - min + 1));
}

int main()
{
    // Seta a seed para gerar números aleatórios
    srand(time(NULL));

    // Definição de variaveis
    int i, j, linhaA, colunaA, linhaB, colunaB, x;

    // Entrada de dados
    linhaA = 835;
    colunaA = 835;
    linhaB = 835;
    colunaB = 835;

    // Definição das matrizes
    float matrizA[linhaA][colunaA], matrizB[linhaB][colunaB], matrizC[linhaA][colunaB], aux;

    if (colunaA == linhaB)
    {
        // Preenche a Matriz A
        for (i = 0; i < linhaA; i++)
        {
            for (j = 0; j < colunaA; j++)
            {
                matrizA[i][j] = sortearNumero(-1000, 1000);
            }
        }

        // Preenche a Matriz B
        for (i = 0; i < linhaB; i++)
        {
            for (j = 0; j < colunaB; j++)
            {
                matrizB[i][j] = sortearNumero(-1000, 1000);
            }
        }
        
        // Imprime as matrizes definidas
        printf("---------------------------- 1 - Matriz Gerada ---------------------------------\n\n");

        for (i = 0; i < linhaA; i++)
        {
            for (j = 0; j < colunaA; j++)
            {
                printf("%6.f", matrizA[i][j]);
            }
            printf("\n\n");
        }

        printf("---------------------------- 2 - Matriz Gerada ---------------------------------\n\n");
        for (i = 0; i < linhaB; i++)
        {
            for (j = 0; j < colunaB; j++)
            {
                printf("%6.f", matrizB[i][j]);
            }
            printf("\n\n");
        }

        printf("---------------------------- 3 - Matriz Gerada ---------------------------------\n\n");

        // Processamento e saida em tela  =  PRODUTO DAS MATRIZES
        for (i = 0; i < linhaA; i++)
        {
            for (j = 0; j < colunaB; j++)
            {

                matrizC[i][j] = 0;
                for (x = 0; x < linhaB; x++)
                {
                    aux += matrizA[i][x] * matrizB[x][j];
                }

                matrizC[i][j] = aux;
                aux = 0;
            }
        }

        for (i = 0; i < linhaA; i++)
        {
            for (j = 0; j < colunaB; j++)
            {
                printf("%6.f", matrizC[i][j]);
            }
            printf("\n\n");
        }
        printf("\n\n");
    }
    else
    {
        printf("\n\n Nao ha como multiplicar as matrizes dadas ");
    }
    return 0;
}
